import { GetServerSidePropsResult } from "next";
import Heading from "../../components/Heading/Heading";
import { ProductDetails } from "../../components/ProductDetails/ProductDetails";
import { IPDPProduct } from "../../types/product";

interface Props {
  product: IPDPProduct
}

export async function getServerSideProps(context): Promise<GetServerSidePropsResult<Props>> {
  const id = context.params.id;
  const product = await fetch(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id,
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'jlp-server',
      },
    }
  ).then(res => res.json()).catch(e => console.error("log error to rum tool", e))

  return {
    props: { product },
  };
}

const ProductDetail = ({ product }: Props) => {
  return (
    <>
      <Heading>{product.title}</Heading>
      <ProductDetails product={product} />
    </>
  );
};

export default ProductDetail;
