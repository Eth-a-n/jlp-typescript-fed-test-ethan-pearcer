import Head from "next/head";
import { GetServerSidePropsResult } from "next";
import productData from '../mockData/productSearch.json'
import { IPLPProduct } from "../types/product";
import Heading from "../components/Heading/Heading";
import { ProductList } from "../components/ProductList/ProductList";

interface Props {
  data: {
    products: IPLPProduct[]
    pageInformation: {
      title: string
      heading: string
    }
    results: number
  }
}

export async function getServerSideProps(): Promise<GetServerSidePropsResult<Props>> {
  const data = productData
  return {
    props: {
      data: data,
    },
  };
}

const PLP = ({ data }: Props) => {
  return (
    <>
      <Head>
        <title>{data.pageInformation.title}</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <Heading>{`${data.pageInformation.heading} (${data.results})`}</Heading>
      <ProductList products={data?.products}/>
    </>
  );
};

export default PLP;
