# UI Dev Technical Test - Dishwasher App

## 3rd Party libaries used
- `html-react-parser` (to parse the `productInfomation` from the API, you could use dangerouslySetInnerHTML but this is safer as should migate any cross site scripting attempts [though I would like think we trust internal apis], this has a small overhead but worth it for the addional security benefit)
- `Jest-axe` (to validate the components have no a11y vilolations, this won't catch them all in reality but isn't a bad start)

## Assumptions
### i18n:
- There is no requirement for i18n (I have hard-coded the titles and currency symbol).

### API Data:
- I have assumed which fields to use in the API to get the required information.

### Browser Support/image loading:
- Only modern browsers are supported, and so the loading attribute on img tags is supported. If this is not the case, we could use a polyfill or, for more control over thresholds, an intersection observer to control when images load on PLP.
- On PDP, for a better customer experience, I have chosen to load all images at once. Otherwise, the experience of waiting for each image to load on scroll might be jarring, though this comes at a perfomance penaltly, there might be a happy medium...

### CSS/styling:
- Spacing, font sizes, and font weights have been estimated based on an 8px rule.
- The smallest desktop viewport is 1024px (I saw a media query in the global CSS).
- The smallest mobile viewport is 320px (around an iPhone SE screen size seemed reasonable).
- Similarly, I have chosen the maximum viewport to be 1440px (I have typically found this to be the maximum).
- CSS variables have been used for colors, but this could be extended to spacing, fonts, etc.
- Used the digital colour meter on mac to get the colours, they moght not exactly match

### Automation tests:
- I didn't have time to add any automation tests, but the assumption is that this is a large ticket (or idealy a set of smaller tickets) and sp would typically take longer than 3 hours and thus, there would be time available to add these.

### SEO:
- No structured data was required, as search engines are fed off a separate product feed (though care was taken to ensure the page is structured so search engines will be able to index the page effectively). However, it would be straightforward to add RDFa attributes to products for search engines to provide richer product information to customers regarding: https://developers.google.com/search/docs/appearance/structured-data/product which would be benefical as google live to have both from freshness reasons
- SEO friendly URLs are to be added at a later time.

### Performance:
- As a non-production-ready app, there has been no performance testing. However, considerations such as choosing when to load images and keeping DOM weight and depth minimal have been taken into account.
- Keeping library usage to a minimum and thus reducing JS weight was also considered.
- Images have been given aspect ratio and height CSS properties, so when loading, there are no CLS issues (checked by the CWV's Chrome extension).

### A11y:
- Did a quick check through with voice over on mac, but would further testing on other A11y tools before fully confident
- Couldn't find unquie alt tags for each image, ideally it would be good to have these for each image, espically those with text in the images
