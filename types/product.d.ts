export interface IPLPProduct {
    image: string;
    productId: string;
    title: string;
    variantPriceRange: {
        display: {
            max: string;
        }
    }
}

export interface IPDPProduct {
    title: string;
    media: {
      images: Iimages
    }
    price: {
      now: string;
    }
    displaySpecialOffer: string;
    additionalServices: {
      includedServices: string[];
    }
    details: {
        productInformation: string,
        features: {
            attributes: {
                name: string;
                value: string;
            }[]
        }[]
    }
    dynamicAttributes: {
        guarantee: string;
    }
    specialOffers: {
        customSpecialOffer?: {
            siteDisplayName?:  string;
        }
    }
    code: string
}

export interface Iimages {
    urls: string[];
}