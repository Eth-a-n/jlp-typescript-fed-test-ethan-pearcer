import { useEffect, useState, useRef } from "react";
import { Iimages } from "../../types/product";
import styles from "./ProductCarousel.module.scss";

interface Props {
  images: Iimages;
  alt: string;
}

const ProductCarousel = ({ images, alt }: Props) => {
  const containerRef = useRef<HTMLUListElement>(null);

  const [state, setState] = useState({
    scroller: null,
    itemWidth: 0,
    currentImage: 0
  });

  const onDotClick = (position) => {
    const diff = position - state.currentImage;
    const scrollPosition = state.scroller.scrollLeft + diff * state.itemWidth;
    setState({ ...state, currentImage: position });
    state.scroller.scrollTo({ left: scrollPosition, behavior: 'smooth' });
  };

  const onScroll = () => {
    const currentPosition = state.scroller.scrollLeft;
    const position = Math.round(currentPosition / state.itemWidth);

    setState({ ...state, currentImage: position });
  }

  useEffect(() => {
    const scroller = containerRef.current;
    const itemWidth = containerRef.current.firstElementChild?.firstElementChild?.clientWidth;

    setState({ ...state, scroller, itemWidth, currentImage: 0 });
  }, [images]);

  return (
    <div className={styles.productCarousel}>
      <ul className={styles.carousel} ref={containerRef} onScroll={onScroll} aria-label="Product carousel">
        {images.urls.map((image, i) => (
            <li key={`image_${i}`} className={styles.imageContainer}>
              <img src={image} alt={`${alt} image ${i + 1}`} className={styles.image} />
            </li>
        ))}
      </ul>
      <ul className={styles.dots} aria-label="Carousel controls">
        {images.urls.map((_, i) => (
            <li key={`dot_${i}`} className={`${styles.dot} ${state.currentImage === i ? styles.activeDot : ''}`} onClick={() => onDotClick(i)} aria-label={`image ${i + 1}`} />
        ))}
      </ul>
    </div>
  );
};

export default ProductCarousel;
