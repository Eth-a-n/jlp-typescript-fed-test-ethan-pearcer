import { render, fireEvent, screen, cleanup, waitFor } from '@testing-library/react';
import ProductCarousel from './ProductCarousel';
import { axe, toHaveNoViolations } from 'jest-axe';

expect.extend(toHaveNoViolations);

describe('ProductCarousel', () => {
  const images = {
    urls: ['image1.jpg', 'image2.jpg', 'image3.jpg'],
  };
  const altText = 'Product Image';

  beforeEach(() => {
    jest.clearAllMocks();
    cleanup();

    Object.defineProperty(HTMLElement.prototype, 'scrollWidth', {
      configurable: true,
      value: 600,
    });
  });

  it('renders correctly with initial state', () => {
    render(<ProductCarousel images={images} alt={altText} />);
    const carousel = screen.getByRole('list', { name: 'Product carousel' });

    expect(carousel).toBeInTheDocument();
    expect(carousel.children.length).toBe(images.urls.length);
  });

  it('clicking on a dot scrolls the carousel', () => {
    render(<ProductCarousel images={images} alt={altText} />);
    const carouselContainer = screen.getByRole('list', { name: 'Product carousel' });
    carouselContainer.scrollTo = jest.fn();

    const dot = screen.getByLabelText('image 2');

    fireEvent.click(dot);

    expect(carouselContainer.scrollLeft).toBe(1 * carouselContainer.firstElementChild.clientWidth);
  });

  it('should have no a11y violations', async () => {
    const { container } = render(<ProductCarousel images={images} alt={altText} />);

    const results = await axe(container);
    expect(results).toHaveNoViolations();
  });
});