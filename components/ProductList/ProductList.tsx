import { IPLPProduct } from "../../types/product";
import ProductListItem from "../ProductListItem/ProductListItem"
import styles from "./ProductList.module.scss";

export const ProductList = ({ products }: { products: IPLPProduct[]}) => (
    <ul className={styles.productList}>
        {products.map((product) => (
            <li key={product.productId} className={styles.item}>
                <ProductListItem {...product} />
            </li>
        ))}
    </ul>
)