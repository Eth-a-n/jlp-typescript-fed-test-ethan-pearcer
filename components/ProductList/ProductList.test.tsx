import { render, screen, cleanup } from '@testing-library/react';
import products from '../../mockData/productSearch.json'
import { ProductList } from './ProductList';
import { axe, toHaveNoViolations } from 'jest-axe';

expect.extend(toHaveNoViolations);

const mockProps = {
    products: products.products
}

describe("ProductList", () => {
  beforeEach(() => {
    jest.clearAllMocks();
    cleanup();
  })

  it("should render",  () => {
    render(<ProductList {...mockProps} />)

    const products = screen.getAllByRole('link')

    expect(products[0]).toBeInTheDocument();
    expect(products.length).toBe(24);
  })

  it('should have no a11y vilolations', async () => {
    const { container } = render(<ProductList {...mockProps} />)

    const results = await axe(container)
    expect(results).toHaveNoViolations()
  })
})
