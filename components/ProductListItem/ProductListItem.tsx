import Link from "next/link";
import styles from "./ProductListItem.module.scss";
import { IPLPProduct } from "../../types/product";

const ProductListItem = ({ image, variantPriceRange, productId, title }: IPLPProduct) => {
  const titleId = `title-${productId}`;
  const priceId = `price-${productId}`;

  return (
    <Link href={`/product-detail/${productId}`}>
      <a aria-labelledby={`${titleId} ${priceId}`}>
        <img src={image} alt={title} className={styles.image} loading="lazy" />
        <div className={styles.info}>
          <p id={titleId}>{title}</p>
          <p id={priceId} className={styles.price}>{variantPriceRange.display.max}</p>
        </div>
      </a>
    </Link>
  );
};

export default ProductListItem;
