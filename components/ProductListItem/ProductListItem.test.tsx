import { render, screen } from '@testing-library/react';
import ProductListItem from "./ProductListItem";
import products from '../../mockData/productSearch.json'
import { axe, toHaveNoViolations } from 'jest-axe';

expect.extend(toHaveNoViolations);

const mockProduct = products.products[0];

describe("ProductListItem", () => {
  it("should render",  () => {
    render(<ProductListItem {...mockProduct} />)

    const product = screen.getByRole('link');

    expect(product).toBeInTheDocument();
  })

  it("should render the title and price",  () => {
    render(<ProductListItem {...mockProduct} />)

    const title = screen.getByText(mockProduct.title);
    const price = screen.getByText(mockProduct.title);

    expect(title).toBeInTheDocument();
    expect(price).toBeInTheDocument();
  })

  it("should render the image",  () => {
    render(<ProductListItem {...mockProduct} />)

    const image = screen.getByAltText(mockProduct.title);

    expect(image).toBeInTheDocument();
  })

  it("should have a correct aria label",  () => {
    render(<ProductListItem {...mockProduct} />)

    const aria = screen.getByLabelText(`${mockProduct.title} ${mockProduct.variantPriceRange.display.max}`)

    expect(aria).toBeInTheDocument();
  })

  it('should have no a11y vilolations', async () => {
    const { container } = render(<ProductListItem {...mockProduct} />)

    const results = await axe(container)
    expect(results).toHaveNoViolations()
  })
})
