import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Heading from './Heading';
import { axe, toHaveNoViolations } from 'jest-axe';

expect.extend(toHaveNoViolations);

describe('Heading', () => {
    const testText = 'Test Heading';
    
    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    })

    it('renders children with correct styles', () => {
        const { getByText } = render(<Heading>{testText}</Heading>);
        const headingElement = getByText(testText);
        expect(headingElement).toBeInTheDocument();
        expect(headingElement).toHaveClass('heading'); // Ensure correct class is applied
    });

    it('should have no a11y vilolations', async () => {
        const { container } = render(<Heading>{testText}</Heading>);
    
        const results = await axe(container)
        expect(results).toHaveNoViolations()
      })
});