import styles from "./Heading.module.scss";

interface Props {
    children: React.ReactNode
}

const Heading = ({ children }: Props) => {
  return (
    <h1 className={styles.heading}>
      {children}
    </h1>
  );
};

export default Heading;
