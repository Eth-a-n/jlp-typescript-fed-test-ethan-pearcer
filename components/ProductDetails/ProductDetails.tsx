import { IPDPProduct } from "../../types/product"
import ProductCarousel from "../ProductCarousel/ProductCarousel"
import parse from 'html-react-parser';
import styles from "./ProductDetails.module.scss";

export const ProductDetails = ({ product }: { product: IPDPProduct}) => {
    const { media, price, specialOffers, details, code, title, dynamicAttributes } = product;
    const productinfo = parse(details.productInformation);

    return (
        <div className={styles.productDetails}>
            <ProductCarousel images={media.images} alt={title}/>
            <div className={styles.info}>
                <h2 className={styles.price}>{`£${price.now}`}</h2>
                {specialOffers.customSpecialOffer && <p className={styles.offer}>{specialOffers.customSpecialOffer.siteDisplayName}</p>}
                <p className={styles.guarantee}>{dynamicAttributes.guarantee}</p>
            </div>
            <div className={styles.specs}>
                <h3 className={styles.subTitle}>Product information</h3>
                <p className={styles.code}>Product code: {code}</p>
                <div className={styles.productinfo}>{productinfo}</div>
                <h3 className={styles.subTitle}>Product specification</h3>
                <ul className={styles.features}>
                    {details.features[0].attributes.map((item) => (
                        <li key={item.name} className={styles.feature}>
                            <p className={styles.name}>{item.name}</p><p className={styles.value}>{item.value}</p>
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    )
}