import React from 'react';
import { render, cleanup, screen, getAllByRole } from '@testing-library/react';
import { ProductDetails } from './ProductDetails';
import product from '../../mockData/product-detail-5561997.json'
import { axe, toHaveNoViolations } from 'jest-axe';

expect.extend(toHaveNoViolations);

describe('ProductDetails', () => {
    beforeEach(() => {
        jest.clearAllMocks();
        cleanup();
    })

    it('renders product images', () => {
        render(<ProductDetails product={product} />);
        const imageElements = screen.getAllByRole('img');
        expect(imageElements).toHaveLength(product.media.images.urls.length);
    });

    it('renders product price', () => {
        render(<ProductDetails product={product} />);
        const priceElement = screen.getByText(`£${product.price.now}`);
        expect(priceElement).toBeInTheDocument();
    });

    it('renders special offer', () => {
        render(<ProductDetails product={product} />);
        const offerElement = screen.getByText(product.specialOffers.customSpecialOffer.siteDisplayName);
        expect(offerElement).toBeInTheDocument();
    });

    it('renders guarantee', () => {
        render(<ProductDetails product={product} />);
        const guaranteeElement = screen.getAllByText(product.dynamicAttributes.guarantee)[0];
        expect(guaranteeElement).toBeInTheDocument();
    });

    it('renders product code', () => {
        render(<ProductDetails product={product} />);
        const codeElement = screen.getByText(`Product code: ${product.code}`);
        expect(codeElement).toBeInTheDocument();
    });

    it('renders product information', () => {
        render(<ProductDetails product={product} />);
        const productInfoElement = screen.getByText('Product information');
        expect(productInfoElement).toBeInTheDocument();
    });

    it('renders product feature', () => {
        render(<ProductDetails product={product} />);
        const featureNameElement = screen.getByText(product.details.features[0].attributes[0].name);
        expect(featureNameElement).toBeInTheDocument();
        const featureValueElement = screen.getByText(product.details.features[0].attributes[0].value);
        expect(featureValueElement).toBeInTheDocument();
    });

    it('should have no a11y vilolations', async () => {
        const { container } = render(<ProductDetails product={product} />)

        const results = await axe(container)
        expect(results).toHaveNoViolations()
     })
});